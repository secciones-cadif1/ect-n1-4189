//debugger;
const EDAD_MINIMA = 18;
const EDAD_MAXIMA = 90;
var edad = Math.random()*(EDAD_MAXIMA-EDAD_MINIMA)+EDAD_MINIMA;

const SUELDO_MINIMO = 50;
const SUELDO_MAXIMO = 1000;
var sueldo = Math.random()*(SUELDO_MAXIMO-SUELDO_MINIMO)+SUELDO_MINIMO;

// se le pide al usuario que introduzca un valor de entrada
var nombre = "jose luis rojas dellan"; //prompt("Introduzca el nombre:")

// se verifica si el usuario hizo click en el boton cancelar
// o si hizo click en aceptar pero no escribio nada
if (nombre == null || nombre == "")
    console.log(`Hizo click en cancelar o no escribio el nombre. 
    Debe escribir el nombre para procesar al empleado`)
else{
    var salida1 = `Su nombre es ${nombre} y su sueldo base es ${sueldo.toFixed(2)}$`

    console.info(salida1)
    console.log(salida1)
    
    var casado = true;
    var porcBono = 0;
    var nuevoSueldo;

    // si el usuario hizo click en el boton Aceptar en el confirm
    if (casado){ // if (casado == true)
        porcBono = 0.25
        console.info(`Por estar casado tendra un bono del ${porcBono*100}%`)
    }else{
        porcBono = 0.1
        console.info(`Por NO estar casado tendra un bono del ${porcBono*100}%`)
    }
     
    var bonoEdad = (edad < 30) ? 25 : 55;

    console.log(edad==30?`Tienes 30 años`:`No tiene 30 años` )

    nuevoSueldo = sueldo + sueldo*porcBono

    console.info(`El original era ${sueldo.toFixed(2)}$, 
        ahora el nuevo sueldo es ${nuevoSueldo.toFixed(2)}$`)

}