
const ProgressBar = require("progress")
var readlineSync = require('readline-sync')

let tope = readlineSync.questionInt("Introduzca el tope de la barra de progreso:" )
let tiempo = readlineSync.questionInt("Introduzca el tiempo de progreso (milisegundos):" )

var bar = new ProgressBar(':bar', { total: tope });
var timer = setInterval(function () {
  bar.tick();
  if (bar.complete) {
    console.log('\nSe ha completado la descarga... ;)\n');
    clearInterval(timer);
  }
}, tiempo);