const {app, BrowserWindow} = require("electron")

console.log("arranco la aplicacion....")

// se programa el evento ready de la app
app.on("ready",()=>{
    createWindow();
    console.log("esta lista la app electron. Su nombre es ")
    console.log(app.name)
    console.log("La aplicacion esta empaquetada:"+app.isPackaged)
    //app.exit();
})

function createWindow () {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
      width: 800,
      height: 600,
    })
}

app.on("before-quit",()=>{
    console.log("se va a cerrar la aplicacion")
})

app.on("quit",()=>{
    console.log("se cerro la aplicacion")
})

console.log("despues del ready..")