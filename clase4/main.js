const {app,BrowserWindow,Menu} = require("electron")
const {plantillaMenu} = require("./menu.js")

let menu = Menu.buildFromTemplate(plantillaMenu)

app.on("ready",()=>{
    Menu.setApplicationMenu(null);
    let main = new BrowserWindow({
        icon: "../html.ico",
        show:false
    });
    main.on("ready-to-show",()=>{
        main.show()
        let item=menu.getMenuItemById("mnuCerrarSoloEstaVentana")
        setInterval(()=>{
            item.enabled=!item.enabled
        },2000)
    })
    main.loadURL("http://gitlab.com");
    main.setMenu(menu)


    /*let otraVent = new BrowserWindow();
    otraVent.loadURL("http://youtube.com");
    otraVent.setMenu(null)*/
})